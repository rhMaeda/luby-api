<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth']], function () {
   
});


Route::get('/alunos', 'AlunoController@all')->name('alunos.all');
Route::post('/alunos', 'AlunoController@create')->name('alunos.create');
Route::get('/alunos/{aluno}', 'AlunoController@show')->name('alunos.show');
Route::put('/alunos/{aluno}', 'AlunoController@update')->name('alunos.update');
Route::delete('/alunos/{aluno}', 'AlunoController@destroy')->name('alunos.destroy');
Route::post('/login', 'LoginController@login')->name('login.login');
Route::post('/signup', 'LoginController@signup')->name('login.signup');