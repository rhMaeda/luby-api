<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $user = DB::table('users')->where(['email' => $request->email,'senha' => $request->senha])->first();
        if($user){
            $user = json_encode($user);
            return response()->json(['message'=> 'Logado com sucesso', 
            'userData' => $user]);
         } else {
            return response()->json(['message'=> 'Falha no login']);
         }

    }

    public function signup(Request $request){
        $email = $request->email;
        $senha = $request->senha;
        try{
            $verifica_email = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9_]+\.([a-zA-Z]{2,4})$~i', $email);
            $verifica_senha = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $senha);
            if(strlen(trim($email)) > 0 && strlen(trim($senha)) > 0 ){
                $usuario = new User;
                $usuario->email = $email;
                $usuario->senha = $senha;
                $usuario->save();
                return response()->json(['message' => 'usuario criado']);
            }
        } catch(error $e){
            echo $e;
        }
        
    }
}