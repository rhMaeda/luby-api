<?php

namespace App\Http\Controllers;

use App\Aluno;
use Illuminate\Http\Request;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $alunos = Aluno::all();
        return response()->json($alunos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $req->validate([
            'nome' => 'required',
            'rg' => 'required',
            'turma' => 'required'
        ]);
        $aluno = Aluno::create($req->all());
        return response()->json(['message'=> 'Aluno criado', 
        'aluno' => $aluno]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function show(Aluno $aluno)
    {
        return $aluno;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function edit(Aluno $aluno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Aluno $aluno)
    {
  
        $req->validate([
            'nome' => 'required',
            'rg' => 'required',
            'turma' => 'required'
        ]);
        $aluno->nome = $req->nome;
        $aluno->rg = $req->rg;
        $aluno->turma = $req->turma;
        $aluno->save();
        return response()->json([
            'message' => 'Aluno atualizado',
            'aluno' => $aluno
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Aluno  $aluno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aluno $aluno)
    {
        $aluno->delete();
        return response()->json([
            'message' => 'Aluno excluido'
        ]);
    }
}
